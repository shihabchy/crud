<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Role;
use DB;

class RoleController extends Controller
{

    public function index()
    {
        // $roles=Role::all();
        $roles=DB::table('roles')->get();

   
        return view('role.index',compact('roles'));
    }

    public function create()
    {
        return view('role.add');
    }

    public function store(Request $request)
{
    $validatedData = $request->validate([
        'role_name' => 'required|unique:roles|max:50',
       
    ]);
  
     $role= Role::create([
  'role_name'=>$request->role_name]);

    if($role)
    {
        return redirect('role/add')->with('success','Data saved successfully');
    }else{
        return redirect('role/add')->with('erroe','Data can not save');
    }


}
}
