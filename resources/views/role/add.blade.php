@extends('master')

@section('content')


   <section class="container-fluid">
<div class="container">
<div class="row justify-content-center">
    @if(session('success'))
    <div class="alert alert-success" role="alert">
        {{session('success')}}
    </div>
    @endif
    @if(session('error'))
    <div class="alert alert-danger" role="alert">
        {{session('error')}}
    </div>
    @endif
    <div class="col-md-12">
        <a href="{{url('role')}}" class="btn btn-primary pull-right mt-5">Back</a>
        </div>

    <div class="col-md-5">
        <form action="{{url('role/store')}}"method="POST">
            @csrf

            <h3 class="form-group">
                Add Role
            </h3>

            <div class="form-group">
                <label for="">RoleName</label>
                <input type="text" class="form-control  @error('role_name') is-invalid @enderror"name="role_name" id="role_name">
                @error('role_name')
                <span class="invalid-feedback" role="alert">

                <strong>{{$message}}</strong>
                @enderror
                </span>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">save</button>
            </div>
        </form>
    </div>
</div>

</div>


   </section>

@endsection