@extends('master')

@section('content')
<section class="content-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                <a href="{{url('role/add')}}" class="btn btn-primary pull-right">Add Role</a>
                </div>
                <table class="table table-bordered table-striped table-hovered mt-5">

                    <thead>
                        <th>Sl</th>
                        <th>Role Name</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @php
                        $i=1;    
                        @endphp
                        @foreach ($roles as $item)
                            
                      
                        <tr>
                        <td>{{$i++}}</td>
                        <td>{{$item->role_name}}</td>
                        <td>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
    
@endsection