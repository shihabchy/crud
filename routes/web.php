<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     $username='shihab';
//     $mobile='01521400201';
//     // return view('home')->with(['name'=>$username,'mobile'=>$mobile]); //transfer data by with method
//     // return view('home',['name'=>$username,'mobile'=>$mobile]);//transfer data using array
//     return view('home',compact('username','mobile'));//transfer data using compact method
// });
// Route::get('/','Homecontroller@index');
// Route::get('create','Homecontroller@create')->name('create');
// Route::get('/', function () {
//     return view('welcome');
    
// });

// Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm');

Route::get('login', 'Auth\LoginController@Login')->name('login');

Route::group(['middleware' =>'auth'],function(){


Route::post('logout', 'Auth\LoginController@Logout')->name('logout');

});
Route::get('dashboard', 'HomeController@index');
Route::get('role', 'RoleController@index');
Route::get('role/add', 'RoleController@create');
Route::post('role/store', 'RoleController@store');